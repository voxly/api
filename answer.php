<?php
/*

MVP Q/A program for Voxly API 

See README.md for details

*/


/* Returns a number between 1 and 0 representing the likelihood of the sentences meaning the same thing */
function similarity($phrase1, $phrase2) {
	// URL escape phrases
	$phrase1 = urlencode($phrase1);
	$phrase2 = urlencode($phrase2);

	$ch = curl_init('http://swoogle.umbc.edu/StsService/GetStsSim?operation=api&phrase1=' . $phrase1 . '&phrase2=' . $phrase2);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$data = curl_exec($ch);
	curl_close($ch);

	return floatval($data);
}



// Let's create an array of question to answer patterns, read from map.txt
$q_and_a = array();

$ifile = fopen('data/map.txt', 'r');
while (($line = fgets($ifile)) !== false) {
	$tokens = explode("\t", $line.trim(' '));
	$k = $tokens[0];
	$v = array_pop($tokens);
	$q_and_a[$k] = $v;
}
fclose($ifile);

$question = $argv[1];

// Default answer to "we don't know the answer"
$answer = 'Whoops! We can\'t find the answer to that question...' . PHP_EOL . 'Maybe try Googling it?';

// Set our base similarity, to prevent too vague of matches
$similarity = 0.4;

foreach ($q_and_a as $q => $a) {
	$sim = similarity($question, $q);

	if ($sim > $similarity) {
		$answer = $a;
		$similarity = $sim;
	}
}

// Need to figure a good way to return this
echo $answer . PHP_EOL;

?>
